package q1;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class Q1Reducer extends Reducer<Text, CompositeWritable, Text, CompositeWritable>{

	public void reduce(Text key, Iterable<CompositeWritable> values, Context ctx) throws IOException, InterruptedException{

	    CompositeWritable out = new CompositeWritable();

	    for (CompositeWritable next : values)
	    {
	        out.merge(next);
	    }

	    ctx.write(key, out);
	}	
}
