package q1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class Q1Mapper extends Mapper<LongWritable, Text, Text, CompositeWritable>{
	private	final int SUMMARY_LEVEL = 100; 
	private final int SEGMENT_ONE = 1;
	private final int SEGMENT_TWO = 2;

	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String line = new String(value.toString());
		
		String state = line.substring(8,10);
		
		String summary = line.substring(10, 13);
		double sumValue = Double.parseDouble(summary);
		
		String segment= line.substring(27, 28);
		double segmentValue = Double.parseDouble(segment);
		
//		System.out.println("summary: " + sumValue + " segmentValue: " + segmentValue);
		
		
		if ((sumValue == SUMMARY_LEVEL) && (segmentValue == SEGMENT_TWO)) {
			String renter = line.substring(1803, 1812);
			double renterSize = Double.parseDouble(renter);
			
			String owner = line.substring(1812, 1821);
			double ownerSize = Double.parseDouble(owner);
			
//			System.out.println("new key");
			context.write(new Text(state), new CompositeWritable(renterSize, ownerSize));
		} 		
		
	}

}
