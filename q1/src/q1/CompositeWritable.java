package q1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

public class CompositeWritable implements Writable{
    
    private double renterSize;
    private double ownerSize;

    public CompositeWritable() {}

    public CompositeWritable(double renterSize, double ownerSize) {
    	this.renterSize = renterSize;
    	this.ownerSize = ownerSize;
//    	System.out.println("seting values");
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        renterSize = in.readDouble();
        ownerSize = in.readDouble();
//        System.out.println("reading fields");
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeDouble(renterSize);
        out.writeDouble(ownerSize);
//        System.out.println("write fields");
    }

    public void merge(CompositeWritable other) {
        this.renterSize += other.renterSize;
        this.ownerSize += other.ownerSize;
//        System.out.println("merge values");
    }

    @Override
    public String toString() {
//    	System.out.println("start tostring");
    	double total = this.renterSize + this.ownerSize;
    	String s = "Rent percentage: " + ((this.renterSize/total) * 100.0) + "\t" + "Owner percentage: " + ((this.ownerSize/total) * 100.0);
//    	System.out.println("end tostring");
        return s;
    }
}
